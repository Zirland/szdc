<?php
include 'header.php';

echo "== MISSING AGENCY ==<br/>";
$query5 = "SELECT DISTINCT agency_id FROM `route` WHERE (agency_id NOT IN (SELECT agency_id FROM agency)) ORDER BY agency_id;";
if ($result5 = mysqli_query($link, $query5)) {
    while ($row5 = mysqli_fetch_row($result5)) {
        $miss_agency = $row5[0];

        echo "$miss_agency > <a href=\"add_agency.php?id=$miss_agency\" target=\"_blank\"> Add agency</a><br/>";
    }
}

echo "== MISSING ROUTES ==<br/>";
$query15 = "SELECT DISTINCT route_id FROM trip WHERE route_id NOT IN (SELECT route_id FROM `route`) ORDER BY route_id;";
if ($result15 = mysqli_query($link, $query15)) {
    while ($row15 = mysqli_fetch_row($result15)) {
        $miss_route = $row15[0];

        echo "$miss_route > <a href=\"add_route.php?id=$miss_route\" target=\"_blank\"> Add route</a><br/>";
    }
}

echo "== MISSING STOP ==<br/>";
$query25 = "SELECT stoptime.stop_id, stop.stop_name, COUNT(*) as pocet FROM stoptime LEFT JOIN `stop` ON stoptime.stop_id = stop.stop_id WHERE stoptime.stop_id IN (SELECT stop_id FROM `stop` WHERE (stop_lat = 0 OR stop_lon = 0)) GROUP BY stoptime.stop_id ORDER BY pocet DESC;";
if ($result25 = mysqli_query($link, $query25)) {
    while ($row25 = mysqli_fetch_row($result25)) {
        $stop_id = $row25[0];
        $stop_name = $row25[1];

        echo "$stop_id $stop_name > <a href=\"add_stop.php?id=$stop_id\" target=\"_blank\"> Add stop</a><br/>";
    }
}

include 'footer.php';
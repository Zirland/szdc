<?php
include 'header.php';

echo "NO STOP<br/>";
$query5 = "SELECT trip_id FROM trip WHERE trip_id NOT IN (SELECT DISTINCT trip_id FROM stoptime WHERE pickup_type IN (0,3));";
if ($result5 = mysqli_query($link, $query5)) {
    while ($row5 = mysqli_fetch_row($result5)) {
        $trip_id = $row5[0];

        echo "<a href=\"tripedit.php?id=$trip_id\">$trip_id</a><br/>";

        $query12 = "DELETE FROM trip WHERE trip_id='$trip_id';";
        $prikaz12 = mysqli_query($link, $query12);
    }
}

echo "ONE STOP<br/>";
$query77 = "SELECT trip_id FROM (SELECT trip_id, count(*) AS pocet FROM stoptime WHERE pickup_type IN (0,3) GROUP BY trip_id) AS pomoc WHERE pocet=1 ORDER BY trip_id;";
if ($result77 = mysqli_query($link, $query77)) {
    while ($row77 = mysqli_fetch_row($result77)) {
        $trip_id = $row77[0];

        echo "<a href=\"tripedit.php?id=$trip_id\">$trip_id</a><br/>";

        $query26 = "DELETE FROM trip WHERE trip_id='$trip_id';";
        $prikaz26 = mysqli_query($link, $query26);

        $query30 = "DELETE FROM stoptime WHERE trip_id = '$trip_id';";
        $prikaz30 = mysqli_query($link, $query30);
    }
}

echo "DUPLICITE STOP<br/>";
$query37 = "SELECT DISTINCT trip_id FROM (SELECT trip_id, stop_sequence, count(*) as pocet FROM stoptime WHERE pickup_type IN (0,3) GROUP BY trip_id, stop_sequence HAVING pocet > 1) AS pom;";
if ($result37 = mysqli_query($link, $query37)) {
    while ($row37 = mysqli_fetch_row($result37)) {
        $trip_id = $row37[0];

        echo "<a href=\"tripedit.php?id=$trip_id\">$trip_id</a><br/>";
    }
}

echo "NO SHAPE<br/>";
$query47 = "SELECT trip_id FROM trip WHERE shape_id = '';";
if ($result47 = mysqli_query($link, $query47)) {
    while ($row47 = mysqli_fetch_row($result47)) {
        $trip_id = $row47[0];

        echo "$trip_id : <a href=\"tripedit.php?id=$trip_id\">Editace</a><br/>";
    }
}

echo "NO TIME<br/>";
$query54 = "DELETE FROM stoptime WHERE (arrival_time = '0' OR departure_time = '0') AND pickup_type = '1' AND drop_off_type = '1';";
$prikaz54 = mysqli_query($link, $query54);

$query57 = "SELECT trip_id FROM stoptime WHERE arrival_time = '0' OR departure_time = '0';";
if ($result57 = mysqli_query($link, $query57)) {
    while ($row57 = mysqli_fetch_row($result57)) {
        $trip_id = $row57[0];

        echo "$trip_id : <a href=\"tripedit.php?id=$trip_id\">Editace</a><br/>";
    }
}

echo "NO TRIP<br/>";
$query117 = "SELECT route_id FROM `route` WHERE active = '1' AND route_id NOT IN (SELECT DISTINCT route_id FROM trip WHERE active = '1');";
if ($result117 = mysqli_query($link, $query117)) {
    while ($row117 = mysqli_fetch_row($result117)) {
        $route_id = $row117[0];

        echo "<a href=\"routeedit.php?id=$route_id\">$route_id</a><br/>";

        $query124 = "UPDATE `route` SET active = '0' WHERE route_id='$route_id';";
        echo "74: $query124<br/>";
        $prikaz124 = mysqli_query($link, $query124);
    }
}

echo "HEADSIGN<br/>";
$query67 = "SELECT trip_id FROM trip WHERE trip_headsign = '';";
if ($result67 = mysqli_query($link, $query67)) {
    while ($row67 = mysqli_fetch_row($result67)) {
        $trip_id = $row67[0];

        $query72 = "SELECT stop_name FROM `stop` WHERE stop_id IN (SELECT stop_id FROM stoptime WHERE trip_id ='$trip_id' AND stop_sequence IN (SELECT max(stop_sequence) FROM stoptime WHERE pickup_type IN (0,3) AND trip_id = '$trip_id'));";
        if ($result72 = mysqli_query($link, $query72)) {
            while ($row72 = mysqli_fetch_row($result72)) {
                $max_name = $row72[0];

                $query77 = "UPDATE trip SET trip_headsign = '$max_name' WHERE trip_id= '$trip_id';";
                echo "78: $query77<br/>";
                //        $prikaz77 = mysqli_query($link, $query77);
                echo "$trip_id > $max_name<br/>";
            }
        }
    }
}

echo "LONGNAME<br/>";
$query87 = "SELECT route_id FROM `route` WHERE (route_long_name LIKE '%– ' OR route_long_name LIKE ' –%') AND active = '1';";
if ($result87 = mysqli_query($link, $query87)) {
    while ($row87 = mysqli_fetch_row($result87)) {
        $route_id = $row87[0];

        $query92 = "SELECT stop_name FROM `stop` WHERE stop_id IN (SELECT stop_id FROM stoptime WHERE trip_id IN (SELECT trip_id FROM trip WHERE route_id='$route_id') AND stop_sequence IN (SELECT min(stop_sequence) FROM stoptime WHERE pickup_type IN (0,3) AND trip_id IN (SELECT trip_id FROM trip WHERE route_id = '$route_id')));";
        if ($result92 = mysqli_query($link, $query92)) {
            while ($row92 = mysqli_fetch_row($result92)) {
                $min_name = $row92[0];
            }
        }

        $query99 = "SELECT stop_name FROM `stop` WHERE stop_id IN (SELECT stop_id FROM stoptime WHERE trip_id IN (SELECT trip_id FROM trip WHERE route_id='$route_id') AND stop_sequence IN (SELECT max(stop_sequence) FROM stoptime WHERE pickup_type IN (0,3) AND trip_id IN (SELECT trip_id FROM trip WHERE route_id = '$route_id')));";
        if ($result99 = mysqli_query($link, $query99)) {
            while ($row99 = mysqli_fetch_row($result99)) {
                $max_name = $row99[0];
            }
        }

        $wholename = "$min_name – $max_name";

        $query108 = "UPDATE `route` SET route_long_name = '$wholename' WHERE route_id = '$route_id';";
        echo "109: $query108<br/>";
        //        $prikaz108 = mysqli_query($link, $query108);

        echo "$route_id > $wholename<br/>";
    }
}

echo "INACTIVE<br/>";
$query132 = "SELECT route_id FROM `route` WHERE active = '0' AND route_id IN (SELECT DISTINCT route_id FROM trip WHERE active= '1');";
if ($result132 = mysqli_query($link, $query132)) {
    while ($row132 = mysqli_fetch_row($result132)) {
        $route_id = $row132[0];

        echo "<a href=\"routeedit.php?id=$route_id\">$route_id</a><br/>";

        $query139 = "UPDATE `route` SET active = '1' WHERE route_id='$route_id';";
        echo "140: $query139<br/>";
        $prikaz139 = mysqli_query($link, $query139);
    }
}

$query145 = "SELECT trip_id, stop_id, arrival_time, count(*) as pocet FROM stoptime GROUP BY trip_id, stop_id, arrival_time HAVING pocet > 1;";
if ($result145 = mysqli_query($link, $query145)) {
    while ($row145 = mysqli_fetch_row($result145)) {
        $trip_id = $row145[0];
        $stop_id = $row145[1];

        $query151 = "SELECT max(stop_sequence) FROM stoptime WHERE trip_id='$trip_id' AND stop_id='$stop_id';";
        if ($result151 = mysqli_query($link, $query151)) {
            while ($row151 = mysqli_fetch_row($result151)) {
                $maxseq = $row151[0];

                $query156 = "DELETE FROM stoptime WHERE trip_id = '$trip_id' AND stop_id = '$stop_id' AND stop_sequence = '$maxseq';";
                echo "$query156<br/>";
                //$prikaz156 = mysqli_query($link, $query156);
            }
        }
    }
}

include 'footer.php';
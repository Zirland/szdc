<?php
date_default_timezone_set('Europe/Prague');
ini_set('memory_limit', '-1');
set_time_limit(0);

require_once 'dbconnect.php';
$link = mysqli_connect($DB_SERVER, $DB_USERNAME, $DB_PASSWORD, $DB_NAME);
if (!$link) {
    echo "Error: Unable to connect to database." . PHP_EOL;
    echo "Reason: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

function Convert($sour)
{
    $deg = substr($sour, 1, 2);
    $min = substr($sour, 5, 2);
    $sec = str_replace(",", ".", substr($sour, 8, 6));
    $value = intval($deg) + intval($min) / 60 + floatval($sec) / 3600;
    return number_format($value, 7, '.', '');
}

if (file_exists("SR70.csv")) {
    $query24 = "TRUNCATE TABLE sr70;";
    $prikaz24 = mysqli_query($link, $query24);

    $file = fopen("SR70.csv", "r");
    fgetcsv($file); // skip first line

    while (!feof($file)) {
        $line = fgetcsv($file, 0, ",");
        $sr_id = $line[0];
        $nazev = $line[1];
        $stav_code = $line[11];
        $gpsX = $line[28];
        $gpsY = $line[29];

        $bod_id = "54{$sr_id}0";

        $bodX = Convert($gpsX);
        $bodY = Convert($gpsY);

        if ($bodX == 0 || $bodY == 0) {
            continue;
        } else {
            $query46 = "INSERT INTO sr70 (nazev, stav, gpsX, gpsY, bod_id) VALUES ('$nazev', '$stav_code', '$bodX', '$bodY', '$bod_id');";
            $prikaz = mysqli_query($link, $query46);
        }
    }

    fclose($file);
    unlink("SR70.csv");

    $query54 = "DROP TABLE sr70diff;";
    $prikaz54 = mysqli_query($link, $query54);

    $query57 = "CREATE TABLE sr70diff AS (SELECT t1.bod_id, t1.nazev, t1.stav, t1.gpsX, t1.gpsY FROM sr70 t1 LEFT JOIN sr70old t2 ON t1.bod_id = t2.bod_id WHERE t2.bod_id IS NULL OR (t1.nazev <> t2.nazev OR t1.stav <> t2.stav OR t1.gpsX <> t2.gpsX OR t1.gpsY <> t2.gpsY));";
    $prikaz57 = mysqli_query($link, $query57);

    $query60 = "ALTER TABLE sr70diff ADD CONSTRAINT sr70diff_pk PRIMARY KEY (bod_id);";
    $prikaz60 = mysqli_query($link, $query60);

    $query63 = "DROP TABLE sr70old;";
    $prikaz63 = mysqli_query($link, $query63);

    $query66 = "CREATE TABLE sr70old AS SELECT * FROM sr70;";
    $prikaz66 = mysqli_query($link, $query66);

    $query69 = "ALTER TABLE sr70old ADD CONSTRAINT sr70old_pk PRIMARY KEY (bod_id);";
    $prikaz69 = mysqli_query($link, $query69);
}
mysqli_close($link);
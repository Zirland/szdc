<?php
include 'header.php';

function getContrastYIQ($hexcolor)
{
    $r = hexdec(substr($hexcolor, 0, 2));
    $g = hexdec(substr($hexcolor, 2, 2));
    $b = hexdec(substr($hexcolor, 4, 2));
    $yiq = ($r * 299 + $g * 587 + $b * 114) / 1000;
    return ($yiq >= 128) ? '000000' : 'FFFFFF';
}

$route = @$_GET['id'];

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $route = $_POST['route_id'];
    $dopravce = $_POST['dopravce'];
    $shortname = $_POST['shortname'];
    $longname = $_POST['longname'];
    $pozadi = $_POST['route_pozadi'];
    $foreground = getContrastYIQ($pozadi);
    $aktif = $_POST['aktif'];

    $ready24 = "UPDATE `route` SET agency_id='$dopravce', route_short_name='$shortname', route_long_name='$longname', route_color='$pozadi', route_text_color='$foreground', active='$aktif' WHERE (route_id = '$route');";
    $aktualz24 = mysqli_query($link, $ready24);
}

echo "<table><tr><td>";
echo "<table>";
echo "<tr>";

$query32 = "SELECT route_id, agency_id, route_short_name, route_long_name, route_type, route_color, route_text_color, active FROM `route` WHERE (route_id='$route');";
if ($result32 = mysqli_query($link, $query32)) {
    while ($row32 = mysqli_fetch_row($result32)) {
        $route_id = $row32[0];
        $agency_id = $row32[1];
        $route_short_name = $row32[2];
        $route_long_name = $row32[3];
        $route_color = $row32[5];
        $route_text_color = $row32[6];
        $route_active = $row32[7];
    }
}

echo "<form method=\"post\" action=\"" . htmlspecialchars($_SERVER['SCRIPT_NAME']) . "\"><input name=\"route_id\" value=\"$route_id\" type=\"hidden\"><input name=\"route_pozadi\" value=\"$route_color\" type=\"hidden\">";
echo "<td>Dopravce: <select name=\"dopravce\">";

$query48 = "SELECT agency_id, agency_name FROM agency ORDER BY agency_id;";
if ($result48 = mysqli_query($link, $query48)) {
    while ($row48 = mysqli_fetch_row($result48)) {
        $agid = $row48[0];
        $agname = $row48[1];

        echo "<option value=\"$agid\"";
        if ($agid == $agency_id) {
            echo " SELECTED";
        }
        echo ">$agname</option>";
    }
}
echo "</select></td><td style=\"background-color : #$route_color;\">Linka: <input type=\"text\" name=\"shortname\" size=\"10\" value=\"$route_short_name\"><br />";
echo "<input type=\"text\" name=\"longname\" value=\"$route_long_name\"></td>";

echo "<td>Aktivní <input type=\"checkbox\" name=\"aktif\" value=\"1\"";
if ($route_active == '1') {
    echo " CHECKED";
}
echo "></td><td><input type=\"submit\"></td></tr></form></table>";

echo "<table>";
echo "<tr><th>Linky odchozí</th><th>Linky příchozí</th></tr>";
echo "<tr><td>";

$query74 = "SELECT trip_id,trip_headsign,active FROM trip WHERE ((route_id = '$route_id') AND (direction_id = '0')) ORDER BY trip_id;";
if ($result74 = mysqli_query($link, $query74)) {
    $count = mysqli_num_rows($result74);
    echo "$count<br/>";
    while ($row74 = mysqli_fetch_row($result74)) {
        $trip_id = $row74[0];
        $trip_headsign = $row74[1];
        $trip_split = explode("~", $trip_id);
        $vlak = $trip_split[0];
        $trip_aktif = $row74[2];

        $query85 = "SELECT stop_name FROM `stop` WHERE stop_id IN (SELECT stop_id FROM stoptime WHERE trip_id = '$trip_id' AND stop_sequence IN (SELECT min(stop_sequence) FROM stoptime WHERE trip_id = '$trip_id' AND pickup_type IN (0,3)));";
        $result85 = mysqli_query($link, $query85);
        $pomhead = mysqli_fetch_row($result85);
        $from = $pomhead[0];

        if ($trip_aktif == '1') {
            echo "<span style=\"background-color:#54FF00;\">";
        }
        echo "$from - ";
        echo "$vlak - $trip_headsign - <a href=\"tripedit.php?id=$trip_id\">Upravit</a>";
        if ($trip_aktif == '1') {
            echo "</span>";
        }
        echo "<br/>";
    }
}
echo "</td><td>";

$query103 = "SELECT trip_id, trip_headsign, active FROM trip WHERE ((route_id = '$route_id') AND (direction_id = '1')) ORDER BY trip_id;";
if ($result103 = mysqli_query($link, $query103)) {
    $count = mysqli_num_rows($result103);
    echo "$count<br/>";
    while ($row103 = mysqli_fetch_row($result103)) {
        $trip_id = $row103[0];
        $trip_headsign = $row103[1];
        $trip_split = explode("~", $trip_id);
        $vlak = $trip_split[0];
        $trip_aktif = $row103[2];

        $query114 = "SELECT stop_name FROM `stop` WHERE stop_id IN (SELECT stop_id FROM stoptime WHERE trip_id = '$trip_id' AND stop_sequence IN (SELECT min(stop_sequence)FROM stoptime WHERE trip_id = '$trip_id' AND pickup_type IN (0,3)));";
        $result114 = mysqli_query($link, $query114);
        $pomhead = mysqli_fetch_row($result114);
        $from = $pomhead[0];

        if ($trip_aktif == '1') {
            echo "<span style=\"background-color:#54FF00;\">";
        }
        echo "$from - ";
        echo "$vlak - $trip_headsign - <a href=\"tripedit.php?id=$trip_id\">Upravit</a>";
        if ($trip_aktif == '1') {
            echo "</span>";
        }
        echo "<br/>";
    }
}
echo "</td></tr></table>";

include 'footer.php';
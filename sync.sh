#!/bin/bash
CURR_YEAR=`date +"%Y"`
CURR_MON=`date +"%Y-%m"`
LAST_MON=`date +"%Y-%m" -d 'last month'`


cd /var/www/szdc/
wget -m -N ftp://ftp.cisjr.cz/draha/celostatni/szdc/

cd ftp.cisjr.cz/draha/celostatni/szdc/$CURR_YEAR
find *.zip -newer /var/www/szdc/timestamp -exec 7z x -aoa {} \;

cd $LAST_MON
find *.zip -newer /var/www/szdc/timestamp -exec 7z x -aoa {} \;
cd ..

cd $CURR_MON
find *.zip -newer /var/www/szdc/timestamp -exec 7z x -aoa {} \;

cd /var/www/szdc/
cd ftp.cisjr.cz/draha/celostatni/szdc/2024
cd $CURR_MON
find *.zip -newer /var/www/szdc/timestamp -exec 7z x -aoa {} \;

cd /var/www/szdc/
touch timestamp

exit;

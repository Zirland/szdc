<?php
include 'header.php';

$action = @$_POST['action'];
$filtr = @$_POST['filtr'];

echo "<form method=\"post\" action=\"station.php\" name=\"filtr\"><input name=\"action\" value=\"filtr\" type=\"hidden\">";
echo "<select name=\"filtr\">";
$query9 = "SELECT stop_id, stop_name FROM `stop` WHERE stop_id IN (SELECT DISTINCT stop_id FROM stoptime WHERE pickup_type IN (0,3) AND drop_off_type IN (0,3)) ORDER BY stop_name;";
if ($result9 = mysqli_query($link, $query9)) {
    while ($row9 = mysqli_fetch_row($result9)) {
        $kod = $row9[0];
        $nazev = $row9[1];

        echo "<option value=\"$kod\"";
        if ($kod == $filtr) {
            echo " SELECTED";
        }
        echo ">$nazev</option>";
    }
    mysqli_free_result($result9);
}
echo "</select>";
echo "<input type=\"submit\"></form>";

switch ($action) {
    case "filtr":
        echo "<table border=\"1\">";
        echo "<tr>";
        echo "<th>Vlak</th>
		<th>Linka</th>
        <th>Dopravce</th>
		<th>Čas</th>
		<th>Cílová stanice</th>
        <th>Poznámka</th>";
        echo "</tr>";

        $x = 0;
        $now = "00:00:00";
        $end = "48:00:00";

        $query42 = "SELECT trip_id FROM stoptime WHERE (stop_id='$filtr' AND departure_time>='$now' AND departure_time<='$end' AND pickup_type IN (0,3) AND drop_off_type IN (0,3)) ORDER BY departure_time;";
        if ($result42 = mysqli_query($link, $query42)) {
            while ($row42 = mysqli_fetch_row($result42)) {
                $trip_id = $row42[0];

                $fixdate = date_create();
                $dnes = date_format($fixdate, 'Y-m-d');

                $prirustek = "63 days";
                date_add($fixdate, date_interval_create_from_date_string($prirustek));
                $konec = date_format($fixdate, 'Y-m-d');

                $matice = "";

                for ($i = 0; $i < 63; $i++) {
                    $matice[$i] = 0;
                }

                $dnes_date = date_create_from_format('Y-m-d', $dnes);

                $query62 = "SELECT datum FROM jizdy WHERE trip_id='$trip_id' AND datum>='$dnes' AND datum<'$konec';";
                if ($result62 = mysqli_query($link, $query62)) {
                    while ($row62 = mysqli_fetch_row($result62)) {
                        $datum = $row62[0];

                        $day_date = date_create_from_format('Y-m-d', $datum);
                        $daydiff = date_diff($dnes_date, $day_date);
                        $dnu = $daydiff->days;

                        $matice[$dnu] = 1;
                    }
                }

                $vtydnu = date_format($dnes_date, "w");

                $weekmatrix = "";

                for ($k = 0; $k < 7; $k++) {
                    $linecount = 0;
                    for ($j = $k; $j < strlen($matice); $j += 7) {
                        $hodnota = (int) $matice[$j];
                        $linecount += $hodnota;
                    }
                    $matrix_value = ($linecount >= 4) ? 1 : 0;
                    $weekmatrix .= $matrix_value;
                }

                $adjust = substr($weekmatrix, -$vtydnu + 1) . substr($weekmatrix, 0, -$vtydnu + 1);
                $dec = bindec($adjust) + 1;

                $service_id = $dec;

                if ($service_id > 1) {
                    $query95 = "SELECT route_id, trip_headsign, wheelchair_accessible, bikes_allowed FROM trip WHERE trip_id = '$trip_id';";
                    if ($result95 = mysqli_query($link, $query95)) {
                        while ($row95 = mysqli_fetch_row($result95)) {
                            $route_id = $row95[0];
                            $trip_headsign = $row95[1];
                            $wheel = $row95[2];
                            $bike = $row95[3];
                        }
                        mysqli_free_result($result95);
                    }

                    echo "<tr>";
                    echo "<td>";
                    echo explode("~", $trip_id)[0];
                    echo "</td>";

                    $query111 = "SELECT route_short_name,route_color,route_text_color,agency_name FROM `route` LEFT JOIN agency ON route.agency_id = agency.agency_id WHERE (route_id = '$route_id') ORDER BY route_short_name;";
                    if ($result111 = mysqli_query($link, $query111)) {
                        while ($row111 = mysqli_fetch_row($result111)) {
                            $route_short_name = $row111[0];
                            $route_color = $row111[1];
                            $route_text_color = $row111[2];
                            $dopravce = $row111[3];
                        }
                        mysqli_free_result($result111);
                    }

                    echo "<td style=\"background-color: #$route_color; text-align: center;\"><span style=\"color: #$route_text_color;\">";
                    echo "$route_short_name</td><td>$dopravce";
                    echo "</td><td>";

                    $query126 = "SELECT departure_time FROM stoptime WHERE (trip_id = '$trip_id' AND stop_id = '$filtr');";
                    if ($result126 = mysqli_query($link, $query126)) {
                        while ($row126 = mysqli_fetch_row($result126)) {
                            $zde = $row126[0];
                        }
                        mysqli_free_result($result126);
                    }

                    $zde = substr($zde, 0, 5);
                    echo $zde;
                    echo "</td>";

                    $query138 = "SELECT trip_headsign FROM trip WHERE trip_id = '$trip_id';";
                    if ($result138 = mysqli_query($link, $query138)) {
                        while ($row138 = mysqli_fetch_row($result138)) {
                            $cil = $row138[0];

                        }
                        mysqli_free_result($result138);
                    }
                    echo "<td>$cil</td>";

                    $wheel_access = ($wheel == 1) ? "&#9855;" : "";
                    $bike_transport = ($bike == 1) ? "&#128690;" : "";

                    match ($service_id) {
                        2 => $calendar = "&#8224;",
                        3 => $calendar = "&#9317;",
                        4 => $calendar = "&#9317; &#8224;",
                        125 => $calendar = "&#9874;",
                        126 => $calendar = "&#9874; &#8224;",
                        127 => $calendar = "&#9874; &#9317;",
                        128 => $calendar = "&#9874; &#9317; &#8224;",
                        default => $calendar = $adjust,
                    };

                    if ($calendar == $adjust) {
                        $calendar = "";
                        if ($adjust[0] == 1) {
                            $calendar .= "&#9312; ";
                        }
                        if ($adjust[1] == 1) {
                            $calendar .= "&#9313; ";
                        }
                        if ($adjust[2] == 1) {
                            $calendar .= "&#9314; ";
                        }
                        if ($adjust[3] == 1) {
                            $calendar .= "&#9315; ";
                        }
                        if ($adjust[4] == 1) {
                            $calendar .= "&#9316; ";
                        }
                        if ($adjust[5] == 1) {
                            $calendar .= "&#9317; ";
                        }
                        if ($adjust[6] == 1) {
                            $calendar .= "&#8224; ";
                        }
                    }

                    echo "<td>$calendar $wheel_access $bike_transport</td>";
                    echo "</tr>";
                    $x++;
                }
            }
            mysqli_free_result($result42);
        }
        echo "</table>";

        echo "<input type=\"hidden\" name=\"pocet\" value=\"$x-1\">";
        echo "<input type=\"submit\">";
        echo "</form>";
        break;
}

include 'footer.php';

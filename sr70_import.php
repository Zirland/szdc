<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
    <meta content="text/html; charset=utf-8" http-equiv="content-type">
    <title>SR70 import</title>
    <script type="text/javascript" src="apikey.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css"
        integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY=" crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
        integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>
    <style>
        #map {
            width: 1200px;
            height: 800px;
        }
    </style>
</head>

<body>
    <?php
    date_default_timezone_set('Europe/Prague');
    ini_set('memory_limit', '-1');
    set_time_limit(0);

    require_once 'dbconnect.php';
    $link = mysqli_connect($DB_SERVER, $DB_USERNAME, $DB_PASSWORD, $DB_NAME);
    if (!$link) {
        echo "Error: Unable to connect to database." . PHP_EOL;
        echo "Reason: " . mysqli_connect_error() . PHP_EOL;
        exit;
    }

    function tab_body($bod_id, $action, $new_name, $lat, $lon, $active, $old_name)
    {
        echo "<table border=\"1\">";
        echo "<tr><td colspan=\"2\">Update stop $bod_id | $old_name</td>";
        echo "<form method=\"post\" action=\"" . htmlspecialchars($_SERVER['SCRIPT_NAME']) . "\"><input name=\"stopid\" value=\"$bod_id\" type=\"hidden\">";
        echo "<td><input name=\"action\" value=\"$action\" type=\"text\"></td></tr>";
        echo "<tr><td>Stop name</td><td>Latitude ~50.123456</td><td>Longitude ~16.987654</td></tr>";
        echo "<tr><td><input name=\"stopname\" id=\"stopname\" value=\"$new_name\" type=\"text\"></td><td><input name=\"stoplat\" id=\"stoplat\" value=\"$lat\" type=\"text\"></td><td><input name=\"stoplon\" id=\"stoplon\" value=\"$lon\" type=\"text\"></td></tr>";
        echo "<tr><td><input name=\"stopactive\" value=\"1\" type=\"checkbox\"";
        if ($active == 1) {
            echo " CHECKED";
        }
        echo "></td><td colspan=\"2\"><input type=\"submit\" value=\"$action\"></form></td></tr>";
        echo "</table>";

        echo "<div id=\"map\"></div>";
    }

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $stop_id = $_POST['stopid'];
        $stop_name = $_POST['stopname'];
        $stop_lat = substr($_POST['stoplat'], 0, 10);
        $stop_lon = substr($_POST['stoplon'], 0, 10);
        $stop_active = @$_POST['stopactive'];
        $stop_active = ($stop_active == 1) ? 1 : 0;
        $action = $_POST['action'];

        $query61 = match ($action) {
            'insert' => "INSERT INTO `stop` (stop_id, stop_name, stop_lat, stop_lon, location_type, parent_station, wheelchair_boarding, active) VALUES ('$stop_id', '$stop_name', '$stop_lat', '$stop_lon', '0', '', '0', '$stop_active');",
            'update' => "UPDATE `stop` SET stop_name = '$stop_name', stop_lat = '$stop_lat', stop_lon = '$stop_lon', active = '$stop_active' WHERE stop_id = '$stop_id';",
        };

        $prikaz61 = mysqli_query($link, $query61);
        if ($prikaz61) {
            $query68 = "SELECT du_id, stop1, stop2 FROM du WHERE stop1 = '$stop_id' OR stop2 = '$stop_id';";
            if ($result68 = mysqli_query($link, $query68)) {
                while ($row68 = mysqli_fetch_row($result68)) {
                    $du_id = $row68[0];
                    $stop1 = $row68[1];
                    $stop2 = $row68[2];

                    $query75 = "SELECT stop_lat, stop_lon FROM `stop` WHERE (stop_id = '$stop1');";
                    if ($result75 = mysqli_query($link, $query75)) {
                        while ($row75 = mysqli_fetch_row($result75)) {
                            $begin_lat = substr($row75[0], 0, 10);
                            $begin_lon = substr($row75[1], 0, 10);
                        }
                    }

                    $query83 = "SELECT stop_lat, stop_lon FROM `stop` WHERE (stop_id = '$stop2');";
                    if ($result83 = mysqli_query($link, $query83)) {
                        while ($row83 = mysqli_fetch_row($result83)) {
                            $end_lat = substr($row83[0], 0, 10);
                            $end_lon = substr($row83[1], 0, 10);
                        }
                    }

                    $cesta = "$begin_lon,$begin_lat;$end_lon,$end_lat";

                    $query93 = "UPDATE du SET `path` = '$cesta' WHERE du_id = $du_id;";
                    $prikaz93 = mysqli_query($link, $query93);
                }
            }

            $query98 = "UPDATE shapetvary SET complete = '0' WHERE (tvartrasy LIKE '%$stop_id%');";
            $prikaz98 = mysqli_query($link, $query98);

            $query101 = "DELETE FROM sr70diff WHERE bod_id = '$stop_id';";
            $prikaz101 = mysqli_query($link, $query101);

            echo "Záznam vložen úspěšně.";
        }
    }

    $id_bod = @$_GET['id'];
    $query109 = ($id_bod != '') ? "SELECT bod_id, nazev, stav, gpsX, gpsY FROM sr70diff WHERE bod_id = $id_bod;" : "SELECT bod_id, nazev, stav, gpsX, gpsY FROM sr70diff ORDER BY bod_id LIMIT 1;";
    if ($result109 = mysqli_query($link, $query109)) {
        while ($row109 = mysqli_fetch_row($result109)) {
            $bod_id = $row109[0];
            $nazev = $row109[1];
            $stav_code = $row109[2];
            $gpsX = $row109[3];
            $gpsY = $row109[4];

            switch ($stav_code) {
                case "4": // Dopravní bod je zrušen
                    $query120 = "SELECT count(*) FROM stoptime WHERE stop_id = '$bod_id';";
                    if ($result120 = mysqli_query($link, $query120)) {
                        while ($row120 = mysqli_fetch_row($result120)) {
                            $pocet = $row120[0];

                            if ($pocet > 0) {
                                echo "Bod je zrušen, ale je v jízdech.<br/>";
                                echo "$nazev, $stav_code, $gpsX, $gpsY, $bod_id | $pocet<br/>";
                            } else {
                                $query128 = "DELETE FROM `stop` WHERE stop_id = '$bod_id';";
                                $prikaz128 = mysqli_query($link, $query128);

                                $query131 = "DELETE FROM sr70diff WHERE bod_id = '$bod_id';";
                                $prikaz131 = mysqli_query($link, $query131);

                                echo "<script>setTimeout(function() {location.reload(); }, 3000);</script>";
                            }
                        }
                    }
                    break;

                default: // Dopravní bod je existující
                    $query141 = "SELECT stop_name, stop_lat, stop_lon, active FROM `stop` WHERE stop_id = '$bod_id';";
                    if ($result141 = mysqli_query($link, $query141)) {
                        $pocet = mysqli_num_rows($result141);
                        if ($pocet > 0) {
                            while ($row141 = mysqli_fetch_row($result141)) {
                                $stop_name = $row141[0];
                                $stop_lat = substr($gpsY, 0, 10);
                                $stop_lon = substr($gpsX, 0, 10);
                                $stop_active = $row141[3];
                            }
                            tab_body($bod_id, "update", $nazev, $stop_lat, $stop_lon, $stop_active, $stop_name);
                        } else {
                            $stop_lat = substr($gpsY, 0, 10);
                            $stop_lon = substr($gpsX, 0, 10);
                            tab_body($bod_id, "insert", $nazev, $stop_lat, $stop_lon, 0, '');
                        }
                    }
            }
        }
    }
    ?>

    <script type="text/javascript">
        function moveMarker(e) {
            let coords = e.target.getLatLng();
            let souradnice = coords.toString().split(', ');
            let souradnice_x = souradnice[0].replace(/LatLng\(/g, '');
            let souradnice_y = souradnice[1].replace(/\)/g, '');

            document.getElementById('stoplat').value = souradnice_x;
            document.getElementById('stoplon').value = souradnice_y;

            map.panTo([souradnice_x, souradnice_y]);
        }

        <?php
        if (isset($stop_lon) && isset($stop_lat)) {
            echo "const init_pos = [$stop_lat, $stop_lon];";
        } else {
            echo 'const init_pos = [50.08, 14.41];';
        }
        ?>
        const map = L.map('map').setView(init_pos, 18);
        const tileLayers = {
            'Základní': L.tileLayer(
                `https://api.mapy.cz/v1/maptiles/basic/256/{z}/{x}/{y}?apikey=${API_KEY}`,
                {
                    minZoom: 0,
                    maxZoom: 19,
                    attribution:
                        '<a href="https://api.mapy.cz/copyright" target="_blank">&copy; Seznam.cz a.s. a další</a>',
                }
            ),
            'Letecká': L.tileLayer(
                `https://api.mapy.cz/v1/maptiles/aerial/256/{z}/{x}/{y}?apikey=${API_KEY}`,
                {
                    minZoom: 0,
                    maxZoom: 20,
                    attribution:
                        '<a href="https://api.mapy.cz/copyright" target="_blank">&copy; Seznam.cz a.s. a další</a>',
                }
            ),
            'OpenStreetMap': L.tileLayer(
                'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                {
                    maxZoom: 19,
                    attribution:
                        '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                }
            ),
        };

        tileLayers['OpenStreetMap'].addTo(map);
        L.control.layers(tileLayers).addTo(map);

        const LogoControl = L.Control.extend({
            options: {
                position: 'bottomleft',
            },

            onAdd: function (map) {
                const container = L.DomUtil.create('div');
                const link = L.DomUtil.create('a', '', container);

                link.setAttribute('href', 'http://mapy.cz/');
                link.setAttribute('target', '_blank');
                link.innerHTML =
                    '<img src="https://api.mapy.cz/img/api/logo.svg" />';
                L.DomEvent.disableClickPropagation(link);

                return container;
            },
        });

        new LogoControl().addTo(map);

        let marker = L.marker(init_pos, {
            draggable: true,
        }).addTo(map);

        marker.on('dragend', moveMarker);

    </script>
</body>

</html>
<?php
date_default_timezone_set('Europe/Prague');

require_once 'dbconnect.php';
$link = mysqli_connect($DB_SERVER, $DB_USERNAME, $DB_PASSWORD, $DB_NAME);
if (!$link) {
    echo "Error: Unable to connect to database." . PHP_EOL;
    echo "Reason: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$dnes = date("Y-m-d", time());

// Odstranění jízd v minulosti
$query15 = "DELETE FROM jizdy WHERE datum < '$dnes';";
$prikaz15 = mysqli_query($link, $query15);

// Smazání logu v minulosti
$query19 = "DELETE FROM `log` WHERE datumdo < '$dnes';";
$prikaz19 = mysqli_query($link, $query19);

// Odstranění duplicity čísla vlaku v jeden den
$query23 = "SELECT id FROM jizdy LEFT OUTER JOIN (SELECT MAX(id) as RowId, shortname, datum FROM jizdy GROUP BY shortname, datum) as KeepRows ON jizdy.id = KeepRows.RowId WHERE KeepRows.RowId IS NULL;";
if ($result23 = mysqli_query($link, $query23)) {
    while ($row23 = mysqli_fetch_row($result23)) {
        $id = $row23[0];

        $query28 = "DELETE FROM jizdy WHERE id = '$id';";
        $prikaz28 = mysqli_query($link, $query28);
    }
}

// Smazání neveřejných vlaků
$query34 = "DELETE FROM trip WHERE trip_id REGEXP '^[0-9]+';";
$prikaz34 = mysqli_query($link, $query34);

// Odstranění vlaků, které již nemají v kalendáři jizdu
$query38 = "SELECT trip_id FROM trip WHERE trip_id NOT IN (SELECT DISTINCT trip_id FROM jizdy);";
if ($result38 = mysqli_query($link, $query38)) {
    while ($row38 = mysqli_fetch_row($result38)) {
        $trip_id = $row38[0];

        $query43 = "DELETE FROM trip WHERE trip_id = '$trip_id';";
        $prikaz43 = mysqli_query($link, $query43);
    }
}

$query48 = "SELECT DISTINCT trip_id FROM stoptime WHERE trip_id NOT IN (SELECT DISTINCT trip_id FROM jizdy);";
if ($result48 = mysqli_query($link, $query48)) {
    while ($row48 = mysqli_fetch_row($result48)) {
        $trip_id = $row48[0];

        $query53 = "DELETE FROM stoptime WHERE trip_id = '$trip_id';";
        $prikaz53 = mysqli_query($link, $query53);
    }
}

// Deaktivace linek, které neobsahují žádný vlak
$query59 = "SELECT route_id FROM `route` WHERE route_id NOT IN (SELECT DISTINCT route_id FROM trip);";
if ($result59 = mysqli_query($link, $query59)) {
    while ($row59 = mysqli_fetch_row($result59)) {
        $route_id = $row59[0];

        $query64 = "UPDATE `route` SET active= '0' WHERE route_id = '$route_id';";
        $prikaz64 = mysqli_query($link, $query64);
    }
}

// Smazání zastavení neexistujících vlaků
$query70 = "DELETE FROM stoptime WHERE trip_id NOT IN (SELECT trip_id FROM trip);";
$prikaz70 = mysqli_query($link, $query70);

// Smazání prázdných automaticky generovaných linek (integrace S linek)
$query74 = "DELETE FROM `route` WHERE route_id LIKE 'K%' AND active = '0';";
$prikaz74 = mysqli_query($link, $query74);

// Smazání posunu ve stanici
$query78 = "DELETE FROM du WHERE stop1 = stop2;";
$prikaz78 = mysqli_query($link, $query78);

// Zaniklé úseky
$query82 = "DELETE FROM du WHERE stop1 IN (SELECT bod_id FROM sr70 WHERE stav = '4') OR stop2 IN (SELECT bod_id FROM sr70 WHERE stav = '4');";
$prikaz82 = mysqli_query($link, $query82);

echo "Hotovo ...";

mysqli_close($link);
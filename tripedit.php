<style>
    .popup {
        position: relative;
        display: inline-block;
        cursor: pointer;
    }

    .popup .popuptext {
        visibility: hidden;
        width: 400px;
        background-color: #555;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 8px 0;
        position: absolute;
        z-index: 1;
        bottom: 125%;
        left: 50%;
        margin-left: -100px;
    }

    .popup .popuptext::after {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: #555 transparent transparent transparent;
    }

    .popup .show {
        visibility: visible;
        -webkit-animation: fadeIn 1s;
        animation: fadeIn 1s
    }

    @-webkit-keyframes fadeIn {
        from {
            opacity: 0;
        }

        to {
            opacity: 1;
        }
    }

    @keyframes fadeIn {
        from {
            opacity: 0;
        }

        to {
            opacity: 1;
        }
    }
</style>

<script>
    function myFunction() {
        var popup = document.getElementById("myPopup");
        popup.classList.toggle("show");
    }
</script>

<?php
include 'header.php';

$trip = @$_GET['id'];
$action = @$_POST['action'];

switch ($action) {
    case "hlava":
        $trip = @$_POST['trip_id'];
        $linka = @$_POST['route_id'];
        $smer = @$_POST['smer'];
        $blok = @$_POST['block_id'];
        $invalida = @$_POST['invalida'];
        $cyklo = @$_POST['cyklo'];

        $ready83 = "UPDATE trip SET route_id='$linka', direction_id='$smer', block_id='$blok', wheelchair_accessible='$invalida', bikes_allowed='$cyklo' WHERE (trip_id = '$trip');";
        $aktualz83 = mysqli_query($link, $ready83);

        $skupina = substr($trip, 0, -6);
        $ready87 = "INSERT INTO linky (skupina, route_id) VALUES ('$skupina', '$linka');";
        $aktualz87 = mysqli_query($link, $ready87);
        break;

    case "zastavky":
        $trip = @$_POST['trip_id'];
        $pocet = @$_POST['pocet'];

        for ($y = 0; $y < $pocet; $y++) {
            $ind = $y;
            $arrindex = "arrive$ind";
            $arrival_time = @$_POST[$arrindex];
            $depindex = "leave$ind";
            $departure_time = @$_POST[$depindex];
            $rzmindex = "rezim$ind";
            $rzm = @$_POST[$rzmindex];
            $pickup_type = substr($rzm, 0, 1);
            $drop_off_type = substr($rzm, 1, 1);
            $seqindex = "poradi$ind";
            $stop_sequence = @$_POST[$seqindex];
            $nameindex = "stopname$ind";
            $stop_name = @$_POST[$nameindex];
            $stpidindex = "stop_id$ind";
            $stop_id = @$_POST[$stpidindex];
            $stp2idindex = "stop2_id$ind";
            $stop2_id = @$_POST[$stp2idindex];

            $ready114 = "UPDATE stoptime SET arrival_time='$arrival_time', departure_time='$departure_time', pickup_type='$pickup_type', drop_off_type='$drop_off_type' WHERE ((trip_id ='$trip') AND (stop_sequence = '$stop_sequence'));";
            $aktualz114 = mysqli_query($link, $ready114);

            $ready117 = "UPDATE `stop` SET stop_name='$stop_name' WHERE (stop_id ='$stop_id');";
            $aktualz117 = mysqli_query($link, $ready117);
        }
        break;
}

echo "<table><tr><td>";
echo "<table>";
echo "<tr>";

$query127 = "SELECT route_id, trip_id, trip_headsign, direction_id, shape_id, wheelchair_accessible, bikes_allowed, active, block_id FROM trip WHERE (trip_id='$trip');";
if ($result127 = mysqli_query($link, $query127)) {
    while ($row127 = mysqli_fetch_row($result127)) {
        $linka = $row127[0];
        $trip_id = $row127[1];
        $trip_headsign = $row127[2];
        $smer = $row127[3];
        $shape = $row127[4];
        $invalida = $row127[5];
        $cyklo = $row127[6];
        $aktif = $row127[7];
        $blok_id = $row127[8];
        $shortname = substr($trip_id, 0, -10);
    }
}

echo "<td><a href = \"routeedit.php?id=$linka\">Zpět na linku</a> | $blok_id<td>";
echo "<td><form method=\"get\" action=\"tripedit.php\" name=\"id\"><input type=\"text\" name=\"id\" value=\"\"><input type=\"submit\"></form><td>";
echo "<td><a href=\"tripdelete.php?trip=$trip_id\" target=\"_blank\">Smazat trip</a></td>";
echo "</tr><tr>";

echo "<form method=\"post\" action=\"tripedit.php\" name=\"hlava\"><input name=\"action\" value=\"hlava\" type=\"hidden\"><input name=\"trip_id\" value=\"$trip_id\" type=\"hidden\"><input name=\"block_id\" value=\"$blok_id\" type=\"hidden\">";
echo "<td>$trip_id</td><td>Linka: <select name=\"route_id\">";

$query151 = "SELECT route_id, route_short_name, route_long_name FROM `route` ORDER BY route_short_name;";
if ($result151 = mysqli_query($link, $query151)) {
    while ($row151 = mysqli_fetch_row($result151)) {
        $roid = $row151[0];
        $roshname = $row151[1];
        $rolgname = $row151[2];

        echo "<option value=\"$roid\"";
        if ($roid == $linka) {
            echo " SELECTED";
        }
        echo ">$roshname | $rolgname</option>";
    }
}
echo "</select></td><td>Směr: $trip_headsign<br />";
echo "<select name=\"smer\"><option value=\"0\"";
if ($smer == '0') {
    echo " SELECTED";
}
echo ">Odchozí</option><option value=\"1\"";
if ($smer == '1') {
    echo " SELECTED";
}
echo ">Příchozí</option></select></td>";
echo "<td>Invalida: <select name=\"invalida\"><option value=\"0\"";
if ($invalida == '0') {
    echo " SELECTED";
}
echo "></option><option value=\"1\"";
if ($invalida == '1') {
    echo " SELECTED";
}
echo ">Vlak vhodný pro přepravu</option><option value=\"2\"";
if ($invalida == '2') {
    echo " SELECTED";
}
echo ">Vlak neumožňuje přepravu</option></select><br />";
echo "Cyklo: <select name=\"cyklo\"><option value=\"0\"";
if ($cyklo == '0') {
    echo " SELECTED";
}
echo "></option><option value=\"1\"";
if ($cyklo == '1') {
    echo " SELECTED";
}
echo ">Vlak vhodný pro přepravu</option><option value=\"2\"";
if ($cyklo == '2') {
    echo " SELECTED";
}
echo ">Vlak neumožňuje přepravu</option></select>";
echo "</td>";
echo "<td>Aktivní <input type=\"checkbox\" name=\"aktif\" value=\"1\"";
if ($aktif == '1') {
    echo " CHECKED";
}
echo "></td><td><input type=\"submit\"></td></tr></form>";
echo "<tr><td colspan=\"5\">";
echo "</td></tr>";
echo "</table>";
echo "<table>";
echo "<tr><td>";
echo "<table>";
echo "<tr><th>Stanice</th><th>Příjezd</th><th><Odjezd</th><th>Režim</th><th>S &nbsp;B</th></tr>";

echo "<form method=\"post\" action=\"tripedit.php\" name=\"zastavky\"><input name=\"action\" value=\"zastavky\" type=\"hidden\"><input name=\"trip_id\" value=\"$trip_id\" type=\"hidden\">";
$z = 0;

$query218 = "SELECT stoptime.stop_id,stoptime.arrival_time,stoptime.departure_time,stoptime.pickup_type,stoptime.drop_off_type,stoptime.stop_sequence, stop.stop_name FROM stoptime LEFT JOIN `stop` ON stoptime.stop_id = stop.stop_id WHERE (stoptime.trip_id = '$trip') ORDER BY stoptime.stop_sequence;";
if ($result218 = mysqli_query($link, $query218)) {
    while ($row218 = mysqli_fetch_row($result218)) {
        $stop_id = $row218[0];
        $arrival_time = $row218[1];
        $departure_time = $row218[2];
        $pickup_type = $row218[3];
        $drop_off_type = $row218[4];
        $stop_sequence = $row218[5];
        $nazev_stanice = $row218[6];

        echo "<tr><td><input name=\"stop_id$z\" value=\"$stop_id\" type=\"hidden\">";
        echo "<a href=\"stopedit.php?id=$stop_id\" target=\"_blank\">E </a>";
        echo "<input type=\"text\" name=\"stopname$z\" value=\"$nazev_stanice\"></td>";
        echo "<td><input type=\"text\" name=\"arrive$z\" value=\"$arrival_time\"></td>";
        echo "<td><input type=\"text\" name=\"leave$z\" value=\"$departure_time\"></td>";
        echo "<td><select name=\"rezim$z\"><option value=\"00\"></option>";
        echo "<option value=\"01\"";
        if ($pickup_type == 0 && $drop_off_type == 1) {
            echo " SELECTED";
        }
        echo ">Pouze nástup</option>";
        echo "<option value=\"10\"";
        if ($pickup_type == 1 && $drop_off_type == 0) {
            echo " SELECTED";
        }
        echo ">Pouze výstup</option>";
        echo "<option value=\"11\"";
        if ($pickup_type == 1 && $drop_off_type == 1) {
            echo " SELECTED";
        }
        echo ">Vlak nezastavuje</option>";
        echo "<option value=\"33\"";
        if ($pickup_type == 3 && $drop_off_type == 3) {
            echo " SELECTED";
        }
        echo ">Zastavuje na znamení</option>";
        echo "<select></td>";
        echo "<td><input name=\"poradi$z\" value=\"$stop_sequence\" type=\"hidden\"><input type=\"checkbox\" name=\"skip$z\" value=\"1\"><input type=\"checkbox\" name=\"break$z\" value=\"1\"></td></tr>";
        $z += 1;
    }
}

echo "<input type=\"hidden\" name=\"pocet\" value=\"$z\">";
echo "<input type=\"submit\"></form>";
echo "</table></td><td>";

echo "TRASA<br />";
$shape = substr($shape, 0, -1);
$body = explode("|", $shape);
$cnt = 1;
foreach ($body as $prujezd) {
    $query270 = "SELECT stop_name FROM `stop` WHERE stop_id = '$prujezd';";
    if ($result270 = mysqli_query($link, $query270)) {
        while ($row270 = mysqli_fetch_row($result270)) {
            $bod_name = $row270[0];
        }
    }

    echo "$cnt | $prujezd | $bod_name<br/>";
    $cnt += 1;
}

echo "</td></tr>";
echo "</table>";

echo "</td></tr></table>";

echo "JÍZDY<br/>";
$datumy = [];
$query288 = "SELECT datum FROM jizdy WHERE trip_id = '$trip_id';";
if ($result288 = mysqli_query($link, $query288)) {
    while ($row288 = mysqli_fetch_row($result288)) {
        $datumy[] = $row288[0];
    }
}

$matice_start = date("Y-m-d", time());

echo "<table border=\"1\"><tr><td>";
for ($u = 0; $u < 365; $u++) {
    $datum = strtotime($matice_start);
    $datum = strtotime("+$u days", $datum);
    $datum_format = date("d.m.", $datum);
    $datum_compare = date("Y-m-d", $datum);
    $denvtydnu = date('w', $datum);
    if (in_array($datum_compare, $datumy)) {
        echo "<span style=\"background-color:green;\">";
    }
    echo "$datum_format<br />";
    if (in_array($datum_compare, $datumy)) {
        echo "</span>";
    }
    if ($denvtydnu == "0") {
        echo "</td><td>";
    }
}
echo "</td></tr></table>";
echo "<div class=\"popup\" onclick=\"myFunction()\">Regenerace<span class=\"popuptext\" id=\"myPopup\">";
$query317 = "SELECT id,trip_id,`file` FROM `log` WHERE shortname = '$shortname';";
if ($result317 = mysqli_query($link, $query317)) {
    while ($row317 = mysqli_fetch_row($result317)) {
        $id = $row317[0];
        $skupina = substr($row317[1], 0, -6);
        $file = $row317[2];

        echo "<a href=\"regen.php?logid=$id\" target=\"_blank\">$skupina $file</a><br/>";
    }
}
echo "</span></div>";
include 'footer.php';
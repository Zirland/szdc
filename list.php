<?php
include 'header.php';

echo "<table>";
echo "<tr>";
echo "<th>Přepravce</th><th>Linka</th><th>Trasa</th><th>Typ</th><th></th><th></th>";
echo "</tr>";
$query8 = "SELECT route_id, agency_id, route_short_name, route_long_name, route_type, route_color, route_text_color, active FROM `route` WHERE (active = 1) ORDER BY agency_id DESC, route_short_name;";
if ($result8 = mysqli_query($link, $query8)) {
    while ($row8 = mysqli_fetch_row($result8)) {
        $route_id = $row8[0];
        $agency_id = $row8[1];
        $route_short = $row8[2];
        $route_long = $row8[3];
        $route_type = $row8[4];
        $route_color = $row8[5];
        $route_text_color = $row8[6];
        $route_active = $row8[7];

        echo "<tr>";

        $query22 = "SELECT agency_name FROM agency WHERE (agency_id = $agency_id);";
        if ($result22 = mysqli_query($link, $query22)) {
            while ($row22 = mysqli_fetch_row($result22)) {
                $ro_ag = $row22[0];

                echo "<td>$ro_ag</td>";
            }
        }

        echo "<td style=\"background-color: #$route_color; text-align: center;\"><span style=\"color: #$route_text_color;\">$route_short</td>";
        echo "<td";
        if ($route_active == "1") {
            echo " style=\"background-color: #54FF00;\"";
        }
        echo ">$route_long</td>";

        switch ($route_type) {
            case 2:
                echo "<td>vlak</td>";
                break;
            case 3:
                echo "<td>autobus</td>";
                break;
            default:
                echo "<td></td>";
                break;
        }
        echo "<td><a href=\"routeedit.php?id=$route_id\">Detaily</a></td>";
    }
    mysqli_free_result($result);
}
echo "<table>";

include 'footer.php';
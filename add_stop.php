<?php
include 'header.php';

$edit_id = @$_GET['id'];

if ($_SERVER['REQUEST_METHOD'] == "POST") {
	$stop_id = $_POST['stopid'];
	$stop_lat = substr($_POST['stoplat'], 0, 10);
	$stop_lon = substr($_POST['stoplon'], 0, 10);
	$stop_active = @$_POST['stopactive'];
	$stop_active = ($stop_active == 1) ? 1 : 0;

	$query13 = "UPDATE `stop` SET stop_lat = '$stop_lat', stop_lon = '$stop_lon', active = '$stop_active' WHERE stop_id = '$stop_id';";
	$prikaz13 = mysqli_query($link, $query13);
	if ($prikaz13) {
		$query16 = "SELECT du_id, stop1, stop2 FROM du WHERE stop1 = '$stop_id' OR stop2 = '$stop_id';";
		if ($result16 = mysqli_query($link, $query16)) {
			while ($row16 = mysqli_fetch_row($result16)) {
				$du_id = $row16[0];
				$stop1 = $row16[1];
				$stop2 = $row16[2];

				$query23 = "SELECT stop_lat, stop_lon FROM `stop` WHERE (stop_id = '$stop1');";
				if ($result23 = mysqli_query($link, $query23)) {
					while ($row23 = mysqli_fetch_row($result23)) {
						$begin_lat = substr($row23[0], 0, 10);
						$begin_lon = substr($row23[1], 0, 10);
					}
				}

				$query31 = "SELECT stop_lat, stop_lon FROM `stop` WHERE (stop_id = '$stop2');";
				if ($result31 = mysqli_query($link, $query31)) {
					while ($row31 = mysqli_fetch_row($result31)) {
						$end_lat = substr($row31[0], 0, 10);
						$end_lon = substr($row31[1], 0, 10);
					}
				}

				$cesta = "$begin_lon,$begin_lat;$end_lon,$end_lat";

				$query41 = "UPDATE du SET `path` = '$cesta' WHERE du_id = $du_id;";
				$prikaz41 = mysqli_query($link, $query41);
			}
		}

		$query46 = "UPDATE shapetvary SET complete = '0' WHERE (tvartrasy LIKE '%$stop_id%');";
		$prikaz46 = mysqli_query($link, $query46);

		echo "Záznam vložen úspěšně.";
	}
} else if ($edit_id) {
	$query52 = "SELECT stop_name, active FROM `stop` WHERE stop_id = '$edit_id';";
	if ($result52 = mysqli_query($link, $query52)) {
		while ($row52 = mysqli_fetch_row($result52)) {
			$stop_name = $row52[0];
			$stop_active = $row52[1];
		}
	}

	$query60 = "SELECT trip_id FROM stoptime WHERE stop_id='$edit_id' LIMIT 1;";
	if ($result60 = mysqli_query($link, $query60)) {
		while ($row60 = mysqli_fetch_row($result60)) {
			$sample = $row60[0];
		}
	}

	$query67 = "SELECT gpsY, gpsX FROM sr70 WHERE bod_id = '$edit_id';";
	if ($result67 = mysqli_query($link, $query67)) {
		while ($row67 = mysqli_fetch_row($result67)) {
			$stop_lat = $row67[0];
			$stop_lon = $row67[1];
		}
	}

	echo "<table>";
	echo "<tr><td colspan=\"2\">Update stop $stop_name | <a href=\"tripedit.php?id=$sample\" target=\"_blank\">Sample trip</a></td></tr>";

	echo "<form method=\"post\" action=\"" . htmlspecialchars($_SERVER['SCRIPT_NAME']) . "\"><input name=\"stopid\" value=\"$edit_id\" type=\"hidden\">";

	echo "<tr><td>Latitude ~50.123456</td><td>Longitude ~16.987654</td></tr>";
	echo "<tr>
	<td><input name=\"stoplat\" id=\"stoplat\" value=\"$stop_lat\" type=\"text\"></td>
	<td><input name=\"stoplon\" id=\"stoplon\" value=\"$stop_lon\" type=\"text\"></td>
	</tr>";
	echo "<tr><td><input name=\"stopactive\" value=\"1\" type=\"checkbox\"";
	if ($stop_active == 1) {
		echo " CHECKED";
	}
	echo "></td><td colspan=\"2\"><input type=\"submit\" value=\"Insert\"></form></td></tr>";
	echo "</table>";
	?>

		<div id="map"></div>

		<script type="text/javascript">
			function moveMarker(e) {
				let coords = e.target.getLatLng();
				let souradnice = coords.toString().split(', ');
				let souradnice_x = souradnice[0].replace(/LatLng\(/g, '');
				let souradnice_y = souradnice[1].replace(/\)/g, '');

				document.getElementById('stoplat').value = souradnice_x;
				document.getElementById('stoplon').value = souradnice_y;

				map.panTo([souradnice_x, souradnice_y]);
			}

			<?php
			if (isset($stop_lon) && isset($stop_lat)) {
				echo "const init_pos = [$stop_lat, $stop_lon];";
			} else {
				echo 'const init_pos = [50.08, 14.41];';
			}
			?>
			const map = L.map('map').setView(init_pos, 18);
			const tileLayers = {
				'Základní': L.tileLayer(
					`https://api.mapy.cz/v1/maptiles/basic/256/{z}/{x}/{y}?apikey=${API_KEY}`,
					{
						minZoom: 0,
						maxZoom: 19,
						attribution:
							'<a href="https://api.mapy.cz/copyright" target="_blank">&copy; Seznam.cz a.s. a další</a>',
					}
				),
				'Letecká': L.tileLayer(
					`https://api.mapy.cz/v1/maptiles/aerial/256/{z}/{x}/{y}?apikey=${API_KEY}`,
					{
						minZoom: 0,
						maxZoom: 20,
						attribution:
							'<a href="https://api.mapy.cz/copyright" target="_blank">&copy; Seznam.cz a.s. a další</a>',
					}
				),
				'OpenStreetMap': L.tileLayer(
					'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
					{
						maxZoom: 19,
						attribution:
							'&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
					}
				),
			};

			tileLayers['OpenStreetMap'].addTo(map);
			L.control.layers(tileLayers).addTo(map);

			const LogoControl = L.Control.extend({
				options: {
					position: 'bottomleft',
				},

				onAdd: function (map) {
					const container = L.DomUtil.create('div');
					const link = L.DomUtil.create('a', '', container);

					link.setAttribute('href', 'http://mapy.cz/');
					link.setAttribute('target', '_blank');
					link.innerHTML =
						'<img src="https://api.mapy.cz/img/api/logo.svg" />';
					L.DomEvent.disableClickPropagation(link);

					return container;
				},
			});

			new LogoControl().addTo(map);

			var marker = L.marker(init_pos, {
				draggable: true,
			}).addTo(map);

			marker.on('dragend', moveMarker);
		</script>

	<?php
} else {
	echo "Není vyžadována žádná akce.";
}

include 'footer.php';
?>
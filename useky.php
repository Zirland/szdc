<?php
include 'header.php';

$file = 'useky.csv';
$current = "";
$i = 0;

$current .= "usek,S1,S2,lat,lon,seq,final\n";

$query10 = "SELECT du_id, stop1, stop2, `path`, final FROM du;";
if ($result10 = mysqli_query($link, $query10)) {
	while ($row10 = mysqli_fetch_row($result10)) {
		$du_id = $row10[0];
		$stop1 = $row10[1];
		$stop2 = $row10[2];
		$linie = $row10[3];
		$final = $row10[4];

		$body = explode(';', $linie);

		foreach ($body as $point) {
			$sourad = explode(',', $point);
			$lat = $sourad[0];
			$lon = $sourad[1];

			if ($lat != '' && $lon != '') {
				$i++;
				$current .= "$du_id,$stop1,$stop2,$lat,$lon,$i,$final\n";
			}
		}
		$i = 0;
	}
}

file_put_contents($file, $current);
include 'footer.php';

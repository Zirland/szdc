<?php
date_default_timezone_set('Europe/Prague');
ini_set('memory_limit', '-1');
set_time_limit(0);

// barva písma bude kontrastní - bílá nebo černá - podle barvy podkladu
function getContrastYIQ($hexcolor)
{
    $r = hexdec(substr($hexcolor, 0, 2));
    $g = hexdec(substr($hexcolor, 2, 2));
    $b = hexdec(substr($hexcolor, 4, 2));
    $yiq = ($r * 299 + $g * 587 + $b * 114) / 1000;
    return ($yiq >= 128) ? '000000' : 'FFFFFF';
}

// výpočet kontrolní číslice ID drážního bodu
function SelfCheck($Code)
{
    $arr = str_split($Code, 1);
    $j = 1;
    $souc = 0;
    foreach ($arr as $num) {
        if (($j % 2) == 1) {
            $num = (int) $num * 2;
        }
        if ($num > 9) {
            $rozp = str_split($num, 1);
            $num = $rozp[0] + $rozp[1];
        }
        $souc += $num;
        $j++;
    }
    $kontr = 100 - $souc;
    while ($kontr > 9) {
        $kontr -= 10;
    }
    return $kontr;
}

$staty = [
    "AT" => "81",
    "BY" => "21",
    "CZ" => "54",
    "DE" => "80",
    "FR" => "87",
    "HU" => "55",
    "IT" => "83",
    "PL" => "51",
    "RU" => "20",
    "SK" => "56",
];

$barvy = [
    "rj" => "008000",
    "SC" => "008000",
    "EC" => "008000",
    "IC" => "008000",
    "EN" => "B51741",
    "NJ" => "008000",
    "ES" => "008000",
    "Ex" => "008000",
    "LE" => "000000",
    "LET" => "000000",
    "RJ" => "ECAE01",
    "R" => "B51741",
    "Sp" => "0094DE",
    "Os" => "0094DE",
    "TLX" => "0094DE",
    "TL" => "0094DE",

    "Rx" => "008000",
    "AEx" => "008983",
    "" => "FFFFFF",
];

$commerce = [
    "50" => "EC",
    "63" => "IC",
    "69" => "Ex",
    "70" => "EN",
    "84" => "Os",
    "94" => "SC",
    "122" => "Sp",
    "157" => "R",
    "209" => "rj",
    "9000" => "Rx",
    "9001" => "TLX",
    "9002" => "TL",
    "9003" => "LE",
    "9004" => "RJ",
    "9005" => "AEx",
    "9006" => "NJ",
    "9007" => "LET",
    "9010" => "ES",

    "11" => "Os",
    "C1" => "Ex",
    "C2" => "R",
    "C3" => "Sp",
    "C4" => "",
    "C5" => "",
    "C6" => "",
    "" => "",
];

$error = 0;

require_once 'dbconnect.php';
$link = mysqli_connect($DB_SERVER, $DB_USERNAME, $DB_PASSWORD, $DB_NAME);
if (!$link) {
    echo "Error: Unable to connect to database." . PHP_EOL;
    echo "Reason: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$logid = $_GET["logid"];

$query81 = "SELECT obsah, shortname FROM log WHERE id = '$logid';";
if ($result81 = mysqli_query($link, $query81)) {
    while ($row81 = mysqli_fetch_row($result81)) {
        $obsah = $row81[0];
        $shortname = $row81[1];
    }
}

$instrukce = <<<XML
$obsah
XML;

$xml = simplexml_load_string($instrukce);
$type = $xml->getname();
switch ($type) {
    case 'CZPTTCISMessage':
        $prev_route_id = $trip_id = $route_id = $headsign = $odd = $trasa = $route_color = "";
        $wheelchair = $bikes = 0;
        $agency_id = $xml->Identifiers->PlannedTransportIdentifiers[1]->Company;
        $Variant = $xml->Identifiers->PlannedTransportIdentifiers[1]->Variant;
        $Locations = $xml->CZPTTInformation->CZPTTLocation;
        $planPA = $xml->Identifiers->PlannedTransportIdentifiers[0];
        $block = "{$planPA->Core}_{$planPA->Variant}_{$planPA->TimetableYear}";

        $centralniPoznamky = [];
        foreach ($xml->NetworkSpecificParameter as $narodniParametr) {
            if ($narodniParametr->Name == "CZCentralPTTNote") {
                $centralniPoznamky[] = explode("|", (string) $narodniParametr->Value)[0];
            }
        }

        $foreignDestination = $xml->CZPTTHeader->CZForeignDestinationLocation->PrimaryLocationName;

        if (in_array("17", $centralniPoznamky)) {
            $wheelchair = 1;
        }

        if (in_array("22", $centralniPoznamky)) {
            $bikes = 1;
        }

        $triplist = [];
        $shortnames = [];

        foreach ($Locations as $locat) {
            $TrafficType = (string) $locat->TrafficType;
            $CommercialTrafficType = (string) $locat->CommercialTrafficType;
            $druh = ($CommercialTrafficType != "") ? $commerce[$CommercialTrafficType] : $commerce[$TrafficType];

            $shortname = (string) $locat->OperationalTrainNumber;

            $Specific = '';
            foreach ($locat->NetworkSpecificParameter as $parametr) {
                if ($parametr->Name == "CZPassengerServiceNumber") {
                    $Specific = (string) $parametr->Value . $agency_id;
                }
            }

            if ($druh != "" || $shortname != "") {
                $shortnames[] = "$druh$shortname~$Specific";
            }
        }

        $shortnames = array_unique($shortnames);

        $StartPeriod = $xml->CZPTTInformation->PlannedCalendar->ValidityPeriod->StartDateTime;
        $EndPeriod = $xml->CZPTTInformation->PlannedCalendar->ValidityPeriod->EndDateTime;
        $datumod = substr($StartPeriod, 0, 10);
        $datumdo = substr($EndPeriod, 0, 10);
        if ($datumdo == "") {
            $datumdo = $datumod;
        }

        $vznik1 = $logid;
        $vznik = $vznik1;
        $vznik = str_pad(substr($vznik, -6), 6, "0", STR_PAD_LEFT);

        $seq = 0;
        $prev_TrafficType = (string) $Locations[0]->TrafficType;
        $prev_CommercialTrafficType = (string) $Locations[0]->CommercialTrafficType;
        $prev_typ = ($prev_CommercialTrafficType != "") ? $commerce[$prev_CommercialTrafficType] : $commerce[$prev_TrafficType];
        $prev_Specific = '';
        $prev_Alternative = '';
        foreach ($Locations[0]->NetworkSpecificParameter as $parametr) {
            if ($parametr->Name == "CZPassengerServiceNumber") {
                $prev_Specific = (string) $parametr->Value . $agency_id;
            }
            if ($parametr->Name == "CZAlternativeTransport") {
                $prev_Alternative = (string) $parametr->Value;
            }
        }
        $prev_part = $part = $haveName = 0;
        $prev_short = $prev_typ . $Locations[0]->OperationalTrainNumber . "~" . $prev_Specific . $prev_Alternative;
        $prev_trip_id = "$prev_short$Variant$prev_part$vznik";

        foreach ($Locations as $lokace) {
            $Activities = [];
            $seq++;
            $Country = (string) $lokace->Location->CountryCodeISO;
            $LocCode = $lokace->Location->LocationPrimaryCode;
            $LocName = $lokace->Location->PrimaryLocationName;
            $train_type = (string) $lokace->TrafficType;
            $com_train_type = (string) $lokace->CommercialTrafficType;
            $shortname = $lokace->OperationalTrainNumber;

            $druh = ($com_train_type != "") ? $commerce[$com_train_type] : $commerce[$train_type];

            // zde neménit
            switch ($shortname % 2) {
                case "0":
                    $odd = "1";
                    break;
                case "1":
                    $odd = "0";
                    break;
            }

            $Specific = '';
            $Alternative = '';
            $Inconsistent = '';
            foreach ($lokace->NetworkSpecificParameter as $parametr) {
                if ($parametr->Name == "CZPassengerServiceNumber") {
                    $Specific = (string) $parametr->Value . $agency_id;
                }
                if ($parametr->Name == "CZAlternativeTransport") {
                    $Alternative = (string) $parametr->Value;
                }
            }

            if ($com_train_type != "" && $shortname != "") {
                $trip_id = $commerce[$com_train_type] . $shortname . "~" . $Specific . $Alternative . $Variant . $part . $vznik;
            } else if ($shortname != "") {
                $trip_id = $commerce[$train_type] . $shortname . "~" . $Specific . $Alternative . $Variant . $part . $vznik;
            }

            if ($haveName == 0) {
                $query254 = "DELETE FROM stoptime WHERE trip_id = '$trip_id';";
                echo "255: $query254<br>";
                $result254 = mysqli_query($link, $query254);
                $haveName = 1;
            }

            $prijezd = 0;
            $odjezd = 0;
            $Timing = $lokace->TimingAtLocation->Timing;
            if ($Timing) {
                foreach ($Timing as $cas) {
                    $TypCasu = $cas->attributes()->TimingQualifierCode;
                    $Hodnota = $cas->Time;
                    $Offset = $cas->Offset;

                    $Hodnota_hod = substr($Hodnota, 0, 2);
                    $Hodnota_rest = substr($Hodnota, 2, 6);
                    $Hodnota_hod = $Offset * 24 + $Hodnota_hod;
                    $Hodnota_hod = str_pad($Hodnota_hod, 2, "0", STR_PAD_LEFT);
                    $Hodnota = "$Hodnota_hod$Hodnota_rest";
                    if ($TypCasu == "ALA") {
                        $prijezd = $Hodnota;
                    }
                    if ($TypCasu == "ALD") {
                        $odjezd = $Hodnota;
                    }
                }
            }
            if ($prijezd == "0" || $Offset < 0) {
                $prijezd = $odjezd;
            }
            if ($odjezd == "0") {
                $odjezd = $prijezd;
            }

            if ($odjezd < $prijezd) {
                $Inconsistent = '1';
            }

            if ($trip_id != $prev_trip_id || $Inconsistent == '1') {
                $trip_start = substr($trip_id, 0, -7);
                $part += 1;
                $trip_end = substr($trip_id, -6);

                $trip_id = "$trip_start$part$trip_end";

                $query300 = "DELETE FROM stoptime WHERE trip_id = '$trip_id';";
                echo "301: $query300<br>";
                $result300 = mysqli_query($link, $query300);
            }

            $Dwell = $lokace->TimingAtLocation->DwellTime;
            $KontrCis = SelfCheck($LocCode);

            $countrcode = $staty[$Country];

            if ($countrcode == "") {
                $countrcode = $Country;
            }

            $stop_id = "{$countrcode}{$LocCode}{$KontrCis}0";
            $trasa .= "$stop_id|";

            $Activities[] = "";
            foreach ($lokace->TrainActivity as $TrainActivity) {
                $Activities[] = $TrainActivity->TrainActivityType;
            }
            $nastup = $vystup = 0;
            if (in_array('0028', $Activities)) {
                $vystup = 1;
            }
            if (in_array('0029', $Activities)) {
                $nastup = 1;
            }
            if (in_array('0030', $Activities)) {
                $nastup = 3;
                $vystup = 3;
            }
            if (!in_array('0001', $Activities)) {
                $nastup = 1;
                $vystup = 1;
            }

            $query337 = "SELECT stop_name FROM `stop` WHERE stop_id = '$stop_id';";
            if ($result337 = mysqli_query($link, $query337)) {
                $hit = mysqli_num_rows($result337);
            }

            if ($hit == 0) {
                $stop_lat = 0;
                $stop_lon = 0;
                $LocName = trim($LocName);
                $acct = (in_array('0001', $Activities)) ? 1 : 0;
                $insert_query = "INSERT INTO `stop` (stop_id, stop_name, stop_lat, stop_lon, location_type, parent_station, wheelchair_boarding, active) VALUES ('$stop_id', '$LocName', '$stop_lat', '$stop_lon', '0', '', '0', $acct);";
                $insert_action = mysqli_query($link, $insert_query);
            }

            $new_prijezd = ($Inconsistent == '1') ? $odjezd : $prijezd;
            $query352 = "INSERT INTO stoptime (trip_id, arrival_time, departure_time, stop_id, stop_sequence, stop_headsign, pickup_type, drop_off_type, shape_dist_traveled, timepoint) VALUES ('$trip_id','$new_prijezd','$odjezd','$stop_id','$seq', '','$nastup','$vystup',0,0);";
            //ignorace času před půlnocí
            if (substr($odjezd, 0, 1) != "-") {
                echo "355: $query352 = $LocName<br/>";
                $prikaz352 = mysqli_query($link, $query352);
                if (in_array('0001', $Activities)) {
                    $headsign = $LocName;
                }
            }

            if ($trip_id != $prev_trip_id) {
                $triplist[] = $prev_trip_id;
                $prev_route_id = "K$prev_trip_id";
                $prev_route_split = explode("~", $prev_route_id);
                if (strlen($prev_route_split[1]) > 10) {
                    $prev_route_id = substr($prev_route_split[1], 0, -10);
                    if (substr($prev_route_split[1], -10, 1) == "1") {
                        $prev_route_id = "B$prev_route_id";
                    }
                }
                $vlak_no = preg_replace('/\D+/', '', $prev_route_split[0]);

                $query374 = "DELETE FROM trip WHERE trip_id='$prev_trip_id';";
                echo "375: $query374<br/>";
                $prikaz374 = mysqli_query($link, $query374);

                $query378 = "INSERT INTO trip (route_id, trip_id, trip_headsign, direction_id, shape_id, wheelchair_accessible, bikes_allowed, active, train_no, block_id) VALUES ('$prev_route_id', '$prev_trip_id', '$headsign', '$odd', '$trasa','$wheelchair', '$bikes', '1', '$vlak_no', '$block');";
                echo "379: $query378<br/>";
                $prikaz378 = mysqli_query($link, $query378);

                $oldstop = 0;
                $old_lon = 0;
                $old_lat = 0;
                $activity = 0;

                $exp_trasa = explode("|", substr($trasa, 0, -1));

                foreach ($exp_trasa as $stop_id) {
                    $query390 = "SELECT final FROM du WHERE stop1 = '$oldstop' AND stop2 = '$stop_id';";
                    if ($result390 = mysqli_query($link, $query390)) {
                        $hit = mysqli_num_rows($result390);
                    }

                    if ($hit == 0) {
                        $stop_lat = 0;
                        $stop_lon = 0;
                        $query398 = "SELECT stop_lat, stop_lon FROM `stop` WHERE stop_id = '$stop_id';";
                        if ($result398 = mysqli_query($link, $query398)) {
                            while ($row398 = mysqli_fetch_row($result398)) {
                                $stop_lat = $row398[0];
                                $stop_lon = $row398[1];
                            }
                        }

                        $prujezdy = "$old_lon,$old_lat;$stop_lon,$stop_lat";

                        if ($oldstop != "0") {
                            $insert_query = "INSERT INTO du (stop1, stop2, `path`, final) VALUES ('$oldstop', '$stop_id', '$prujezdy', '$activity');";
                            echo "410: $insert_query<br />";
                            $insert_action = mysqli_query($link, $insert_query);
                        }
                    }
                    $oldstop = $stop_id;
                    $old_lat = @$stop_lat;
                    $old_lon = @$stop_lon;
                }

                $new_odjezd = ($Inconsistent == '1') ? $prijezd : $odjezd;
                $query420 = "INSERT INTO stoptime (trip_id, arrival_time, departure_time, stop_id, stop_sequence, stop_headsign, pickup_type, drop_off_type, shape_dist_traveled, timepoint) VALUES ('$prev_trip_id','$prijezd','$new_odjezd','$stop_id','$seq', '','$nastup','$vystup',0,0);";
                //ignorace času před půlnocí
                if (substr($odjezd, 0, 1) != "-") {
                    echo "447: $query420 = $LocName<br/>";
                    $prikaz420 = mysqli_query($link, $query420);
                }

                $trasa = "$stop_id|";
                echo "428: Nová trasa<br/>";

                $prev_route_id = $route_id;
                $prev_trip_id = $trip_id;
            }
        }

        $route_id = "K$trip_id";
        $route_split = explode("~", $route_id);
        if (strlen($route_split[1]) > 10) {
            $route_id = substr($route_split[1], 0, -10);
            if (substr($route_split[1], -10, 1) == "1") {
                $route_id = "B$route_id";
            }
        }

        $vlak_no = preg_replace('/\D+/', '', $route_split[0]);

        $triplist[] = $trip_id;
        $query447 = "DELETE FROM trip WHERE trip_id='$trip_id';";
        echo "448: $query447<br/>";
        $prikaz447 = mysqli_query($link, $query447);

        $query451 = "INSERT INTO trip (route_id, trip_id, trip_headsign, direction_id, shape_id, wheelchair_accessible, bikes_allowed, active, train_no, block_id) VALUES ('$route_id', '$trip_id', '$headsign', '$odd', '$trasa','$wheelchair', '$bikes', '1', '$vlak_no', '$block');";
        echo "452: $query451<br/>";
        $prikaz451 = mysqli_query($link, $query451);

        $oldstop = 0;
        $old_lon = 0;
        $old_lat = 0;
        $activity = 0;

        $exp_trasa = explode("|", substr($trasa, 0, -1));

        foreach ($exp_trasa as $stop_id) {
            $query463 = "SELECT final FROM du WHERE stop1 = '$oldstop' AND stop2 = '$stop_id';";
            if ($result463 = mysqli_query($link, $query463)) {
                $hit = mysqli_num_rows($result463);
            }

            if ($hit == 0) {
                $stop_lat = 0;
                $stop_lon = 0;
                $query471 = "SELECT stop_lat, stop_lon FROM `stop` WHERE stop_id = '$stop_id';";
                if ($result471 = mysqli_query($link, $query471)) {
                    while ($row471 = mysqli_fetch_row($result471)) {
                        $stop_lat = $row471[0];
                        $stop_lon = $row471[1];
                    }
                }

                $prujezdy = "$old_lon,$old_lat;$stop_lon,$stop_lat";

                if ($oldstop != "0") {
                    $insert_query = "INSERT INTO du (stop1, stop2, `path`, final) VALUES ('$oldstop', '$stop_id', '$prujezdy', '$activity');";
                    echo "483: $insert_query<br />";
                    $insert_action = mysqli_query($link, $insert_query);
                }
            }
            $oldstop = $stop_id;
            $old_lat = @$stop_lat;
            $old_lon = @$stop_lon;
        }

        $i = 0;
        foreach ($triplist as $route_string) {
            $min_name = $max_name = "";
            $route_id = "K" . $triplist[$i];
            $shortname_expl = explode("~", $route_string);
            $shortname = $shortname_expl[0];
            $triptyp = preg_replace('/\d+/', '', $shortname);
            $bus = substr($shortname_expl[1], -10, 1);
            $route_type = ($bus == '1') ? "3" : "2";
            $route_color = $barvy[$triptyp];
            $textcolor = getContrastYIQ($route_color);

            $query_min = "SELECT stop_name FROM `stop` WHERE stop_id IN (SELECT stop_id FROM stoptime WHERE trip_id IN (SELECT trip_id FROM trip WHERE route_id='$route_id') AND stop_sequence IN (SELECT min(stop_sequence) FROM stoptime WHERE pickup_type IN (0,3) AND trip_id IN (SELECT trip_id FROM trip WHERE route_id='$route_id')));";
            if ($result504 = mysqli_query($link, $query_min)) {
                while ($row504 = mysqli_fetch_row($result504)) {
                    $min_name = $row504[0];
                }
            }

            $query_max = "SELECT stop_name FROM `stop` WHERE stop_id IN (SELECT stop_id FROM stoptime WHERE trip_id IN (SELECT trip_id FROM trip WHERE route_id='$route_id') AND stop_sequence IN (SELECT max(stop_sequence) FROM stoptime WHERE pickup_type IN (0,3) AND trip_id IN (SELECT trip_id FROM trip WHERE route_id='$route_id')));";
            if ($result512 = mysqli_query($link, $query_max)) {
                while ($row512 = mysqli_fetch_row($result512)) {
                    $max_name = $row512[0];
                }
            }

            $wholename = @"$min_name – $max_name";

            $query520 = "DELETE FROM `route` WHERE route_id = '$route_id';";
            echo "521: $query520<br/>";
            $prikaz520 = mysqli_query($link, $query520);

            $query524 = "INSERT INTO `route` (route_id, agency_id, route_short_name, route_long_name, route_type, route_color, route_text_color, active) VALUES ('$route_id', '$agency_id', '$shortname', '$wholename', '$route_type', '$route_color', '$textcolor', '1');";
            echo "525: $query524<br/>";
            $prikaz524 = mysqli_query($link, $query524);
            $i += 1;
        }

        $matice = "";

        $bitmap = $xml->CZPTTInformation->PlannedCalendar->BitmapDays;

        $maticestart = date_create($datumod);
        $timedo = date_create($datumdo);
        $diff = date_diff($maticestart, $timedo);
        $kondnu = $diff->format('%a');

        for ($g = 0; $g < 420; $g++) {
            if ($g == 0) {
                $matice .= $bitmap;
            }
            if ($g > $kondnu) {
                $matice[$g] = 0;
            }
        }

        $relatedPA = $xml->Identifiers->RelatedPlannedTransportIdentifiers[0];
        if ($relatedPA != '') {
            $related_id = "{$relatedPA->Core}_{$relatedPA->Variant}_{$relatedPA->TimetableYear}";
            $query551 = "SELECT trip_id FROM trip WHERE block_id = '$related_id';";
            if ($result551 = mysqli_query($link, $query551)) {
                while ($row551 = mysqli_fetch_row($result551)) {
                    $related_trip_id = $row551[0];

                    $query556 = "DELETE FROM jizdy WHERE trip_id = '$related_trip_id' AND datum >= '$datumod' AND datum <= '$datumdo';";
                    echo "581: $query556<br/>";
                    $prikaz556 = mysqli_query($link, $query556);
                }
            }
        }

        foreach ($triplist as $trip_id) {
            for ($h = 0; $h < 420; $h++) {
                $fixdate = date_create($datumod);
                $prirustek = "$h days";
                date_add($fixdate, date_interval_create_from_date_string($prirustek));
                $totodatum = date_format($fixdate, 'Y-m-d');

                if ($matice[$h] == "1") {
                    $jizda_name = substr($trip_id, 0, -6);
                    $query572 = "INSERT INTO jizdy (shortname, trip_id, datum) VALUES ('$jizda_name', '$trip_id','$totodatum');";
                    echo "573: $query572<br/>";
                    $prikaz572 = mysqli_query($link, $query572);
                }
            }
        }
        break;

    case 'CZCanceledPTTMessage':
        echo "!!!!! HIC SUNT LEONES !!!!!<br/>";
        $error = 1;
        /*            $planPA = $xml->PlannedTransportIdentifiers[0];
        $PA_id  = $planPA->ObjectType . "_" . $planPA->Company . "_" . $planPA->Core . "_" . $planPA->Variant . "_" . $planPA->TimetableYear . ".xml";
        $shortnames[] = '';
        $query414 = "SELECT shortname FROM `log` WHERE file = '$PA_id';";
        echo "634: $query414<br/>";
        if ($result414 = mysqli_query($link, $query414)) {
        while ($row414 = mysqli_fetch_row($result414)) {
        $shortnames[] = $row414[0];
        }
        }
        $StartPeriod = $xml->PlannedCalendar->ValidityPeriod->StartDateTime;
        $EndPeriod   = $xml->PlannedCalendar->ValidityPeriod->EndDateTime;
        $datumod     = substr($StartPeriod, 0, 4) . "-" . substr($StartPeriod, 5, 2) . "-" . substr($StartPeriod, 8, 2);
        $datumdo     = substr($EndPeriod, 0, 4) . "-" . substr($EndPeriod, 5, 2) . "-" . substr($EndPeriod, 8, 2);
        if ($datumdo == "") {
        $datumdo = $datumod;
        }
        $shortnames = array_filter($shortnames);
        foreach ($shortnames as $shortname) {
        $query167 = "INSERT INTO `log` (`file`, shortname, trip_id, datumod, datumdo, obsah) VALUES ('$nazev','$shortname','','$datumod','$datumdo', '$obsah');";
        echo "652: INSERT INTO `log` (`file`, shortname, trip_id, datumod, datumdo, obsah) VALUES ('$nazev','$shortname','','$datumod','$datumdo', '<br/>";
        $obsah_fmt = str_replace("&", "&amp;", $obsah);
        $obsah_fmt = str_replace("<", "&lt;", $obsah_fmt);
        $obsah_fmt = str_replace(">", "&gt;", $obsah_fmt);
        echo "<code>$obsah_fmt</code>";
        $prikaz167 = mysqli_query($link, $query167);
        $matice = "";
        $bitmap = $xml->PlannedCalendar->BitmapDays;
        $Dod         = substr($datumod, 8, 2);
        $Mod         = substr($datumod, 5, 2);
        $Yod         = substr($datumod, 0, 4);
        $timeod      = mktime(0, 0, 0, $Mod, $Dod, $Yod);
        $maticestart = mktime(0, 0, 0, $Mod, $Dod, $Yod);
        $format_od   = date("Y-m-d", $timeod);
        $zacdnu      = round(($timeod - $maticestart) / 86400);
        $Ddo         = substr($datumdo, 8, 2);
        $Mdo         = substr($datumdo, 5, 2);
        $Ydo         = substr($datumdo, 0, 4);
        $timedo      = mktime(0, 0, 0, $Mdo, $Ddo, $Ydo);
        $format_do   = date("Y-m-d", $timedo);
        $kondnu      = round(($timedo - $maticestart) / 86400);
        for ($g = 0; $g < 406; $g++) {
        if ($g < $zacdnu) {
        $matice[$g] = 0;
        }
        if ($g == $zacdnu) {
        $matice .= $bitmap;
        }
        if ($g > $kondnu) {
        $matice[$g] = 0;
        }
        }
        foreach ($shortnames as $shortname) {
        for ($h = 0; $h < 406; $h++) {
        $tentoden  = $maticestart + ($h * 86400);
        $totodatum = date("Y-m-d", $tentoden);
        if ($matice[$h] == "1") {
        $query188 = "DELETE FROM jizdy WHERE shortname = '$shortname' AND datum = '$totodatum';";
        echo "695: $query188<br/>";
        $prikaz188 = mysqli_query($link, $query188);
        }
        }
        }
        } */
        break;
}

mysqli_close($link);
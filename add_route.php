<?php
include 'header.php';

function getContrastYIQ($hexcolor)
{
    $r = hexdec(substr($hexcolor, 0, 2));
    $g = hexdec(substr($hexcolor, 2, 2));
    $b = hexdec(substr($hexcolor, 4, 2));
    $yiq = ($r * 299 + $g * 587 + $b * 114) / 1000;
    return ($yiq >= 128) ? '000000' : 'FFFFFF';
}

$new_id = @$_GET['id'];

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $route_id = $_POST['route_id'];
    $agency_id = $_POST['dopravce'];
    $route_short_name = $_POST['shortname'];
    $route_long_name = $_POST['longname'];
    $route_color = $_POST['route_color'];
    $route_text_color = getContrastYIQ($route_color);
    $route_type = 2;
    $route_active = 1;

    $query25 = "INSERT INTO `route` (route_id,agency_id,route_short_name,route_long_name,route_type,route_color,route_text_color,active) VALUES ('$route_id', '$agency_id', '$route_short_name', '$route_long_name', '$route_type', '$route_color', '$route_text_color', '$route_active');";
    $command25 = mysqli_query($link, $query25);
    if ($command25) {
        $query28 = "SELECT route_id FROM `route` WHERE route_id LIKE 'K%~$route_id%';";
        if ($result28 = mysqli_query($link, $query28)) {
            while ($row28 = mysqli_fetch_row($result28)) {
                $route_del = $row28[0];

                $query33 = "DELETE FROM `route` WHERE route_id = '$route_del';";
                $prikaz33 = mysqli_query($link, $query33);
            }
        }

        echo "Záznam vložen úspěšně.";
    }
} else if ($new_id) {
    echo "<table><tr><td>";
    echo "<table>";
    echo "<tr>";

    $agency_id = substr($new_id, -4);
    $route_short_name = substr($new_id, 0, -4);
    $route_long_name = "";
    $seznam[] = '0094DE';
    $seznam[] = 'B51741';
    $seznam[] = '008000';
    $seznam[] = 'ECAE01';
    $seznam[] = '000000';

    $query54 = "SELECT route_color FROM `route` WHERE route_id LIKE 'K%~$new_id%';";
    if ($result54 = mysqli_query($link, $query54)) {
        while ($row54 = mysqli_fetch_row($result54)) {
            $route_color = $row54[0];

            $seznam[] = $route_color;
        }
    }

    $seznam = array_unique($seznam);
    sort($seznam);

    echo "<form method=\"post\" action=\"" . htmlspecialchars($_SERVER['SCRIPT_NAME']) . "\"><input name=\"route_id\" value=\"$new_id\" type=\"hidden\">";
    echo "<td>Dopravce: <select name=\"dopravce\">";

    $query69 = "SELECT agency_id, agency_name FROM agency ORDER BY agency_id;";
    if ($result69 = mysqli_query($link, $query69)) {
        while ($row69 = mysqli_fetch_row($result69)) {
            $agid = $row69[0];
            $agname = $row69[1];

            echo "<option value=\"$agid\"";
            if ($agid == $agency_id) {
                echo " SELECTED";
            }
            echo ">$agname</option>";
        }
    }
    echo "</select><br/>";
    foreach ($seznam as $barva) {
        $text_barva = getContrastYIQ($barva);
        echo "<input type=\"radio\" name=\"route_color\" value=\"$barva\"><span style=\"background-color:#$barva; color:#$text_barva\">$barva</span><br/>";
    }
    echo "</td><td style=\"background-color : #$route_color;\">Linka: <input type=\"text\" name=\"shortname\" size=\"10\" value=\"$route_short_name\"><br />";
    echo "<input type=\"text\" name=\"longname\" value=\"$route_long_name\"></td>";

    echo "<td></td><td><input type=\"submit\"></td></tr></form></table>";

    echo "<table>";
    echo "<tr><th>Linky odchozí</th><th>Linky příchozí</th></tr>";
    echo "<tr><td>";

    $query96 = "SELECT trip_id,trip_headsign,active FROM trip WHERE ((route_id = '$new_id') AND (direction_id = '0')) ORDER BY trip_id;";
    if ($result96 = mysqli_query($link, $query96)) {
        $count = mysqli_num_rows($result96);
        echo "$count<br/>";
        while ($row96 = mysqli_fetch_row($result96)) {
            $trip_id = $row96[0];
            $trip_headsign = $row96[1];
            $trip_split = explode("~", $trip_id);
            $vlak = $trip_split[0];
            $trip_aktif = $row96[2];

            $query107 = "SELECT stop_name FROM `stop` WHERE stop_id IN (SELECT stop_id FROM stoptime WHERE trip_id = '$trip_id' AND stop_sequence IN (SELECT min(stop_sequence) FROM stoptime WHERE (trip_id = '$trip_id')));";
            $result107 = mysqli_query($link, $query107);
            $pomhead = mysqli_fetch_row($result107);
            $from = $pomhead[0];

            if ($trip_aktif == '1') {
                echo "<span style=\"background-color:#54FF00;\">";
            }
            echo "$from - ";
            echo "$vlak - $trip_headsign";
            if ($trip_aktif == '1') {
                echo "</span>";
            }
            echo "<br/>";
        }
    }
    echo "</td><td>";

    $query125 = "SELECT trip_id, trip_headsign, active FROM trip WHERE ((route_id = '$new_id') AND (direction_id = '1')) ORDER BY trip_id;";
    if ($result125 = mysqli_query($link, $query125)) {
        $count = mysqli_num_rows($result125);
        echo "$count<br/>";
        while ($row125 = mysqli_fetch_row($result125)) {
            $trip_id = $row125[0];
            $trip_headsign = $row125[1];
            $trip_split = explode("~", $trip_id);
            $vlak = $trip_split[0];
            $trip_aktif = $row125[2];

            $query136 = "SELECT stop_name FROM `stop` WHERE stop_id IN (SELECT stop_id FROM stoptime WHERE trip_id = '$trip_id' AND stop_sequence IN (SELECT min(stop_sequence) FROM stoptime WHERE (trip_id = '$trip_id')));";
            $result136 = mysqli_query($link, $query136);
            $pomhead = mysqli_fetch_row($result136);
            $from = $pomhead[0];

            if ($trip_aktif == '1') {
                echo "<span style=\"background-color:#54FF00;\">";
            }
            echo "$from - ";
            echo "$vlak - $trip_headsign";
            if ($trip_aktif == '1') {
                echo "</span>";
            }
            echo "<br/>";
        }
    }
    echo "</td></tr></table>";
} else {
    echo "Není vyžadována žádná akce.";
}

include 'footer.php';
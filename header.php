<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type">
	<title>Železnice</title>
	<script type="text/javascript" src="apikey.js"></script>
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css"
		integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY=" crossorigin="" />
	<script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
		integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>
	<style>
		#map {
			width: 1200px;
			height: 800px;
		}
	</style>
</head>

<body>
	<?php
	date_default_timezone_set('Europe/Prague');
	require_once 'dbconnect.php';
	$link = mysqli_connect($DB_SERVER, $DB_USERNAME, $DB_PASSWORD, $DB_NAME);
	if (!$link) {
		echo "Error: Unable to connect to database." . PHP_EOL;
		echo "Reason: " . mysqli_connect_error() . PHP_EOL;
		exit;
	}
	?>

	<table style="width:100%; height:100%;">
		<tr>
			<td style="background-color:#cccccc;">
				<a href="list.php">&nbsp; &nbsp;</a>
			</td>
			<td style="height:5px; background-color:#cccccc;">
			</td>
		</tr>
		<tr>
			<td style="width:100px; background-color:yellow; vertical-align:top;">
				<a href="cisti.php">Čisti</a><br />
				<a href="opravy.php">Opravy</a><br />
				<a href="missing.php">Missing</a><br />
				<a href="tripedit.php">Tripedit</a><br />
				<a href="station.php">Station</a><br />
			</td>
			<td>